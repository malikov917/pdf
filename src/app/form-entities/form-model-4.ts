/**
 * Created by user1 on 3/26/2017.
 */
// RI Autorisation de prélevementAppsPro last siret

export class FormModel4 {
  // MANDAT DE PRELEVEMENT SEPA
  iban: string = '';
  bic: string = '';
  tiers_debiteur: string = '';
  a: string = '';
  le: string = '';
  ics: string = '';
  rum: string = '';
}
