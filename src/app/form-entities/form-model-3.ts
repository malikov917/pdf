/**
 * Created by user1 on 3/24/2017.
 */

export class FormModel3 {

  // INFOS GÉNÉRALES
  nom_prenom_du_contact: string = '';
  numero_de_portable: string = '';
  numero_de_fixe: string = '';
  mail_ce_mail_recevra_les_demandes_faites_via_lapplication: string = '';
  site_internet: string = '';
  cochez_cette_case_sil_ny_a_pas_de_site_web: boolean = false;
  adresses_facebook: string = '';
  twitter: string = '';
  pinterest: string = '';
  google_plus: string = '';
  instagram: string = '';
  tripadvisor: string = '';
  nom_de_lapplication: string = '';
  mots_cles: string = '';

  // GRAPHISME
  copier_la_charte_graphique_du_site_web: boolean = false;
  copier_la_charte_graphique_du_flyer: boolean = false;
  copier_la_charte_graphique_du_carte_de_visite: boolean = false;
  couleur_1_1: string = '';
  couleur_1_2: string = '';
  couleur_1_3: string = '';
  couleur_1_4: string = '';
  couleur_2_1: string = '';
  couleur_2_2: string = '';
  couleur_2_3: string = '';
  couleur_2_4: string = '';
  couleur_3_1: string = '';
  couleur_3_2: string = '';
  couleur_3_3: string = '';
  couleur_3_4: string = '';
  reprendre_le_logo_du_site_web: boolean = false;
  reprendre_le_logo_du_flyer: boolean = false;
  reprendre_le_logo_du_carte_de_visite: boolean = false;
  creer_un_logo: string = '';
  fonds_decrans: string = '';
  nom_de_la_les_typographies_a_utiliser: string = '';
  remarques: string = '';

  // MODULES
  copier_le_contenu_et_lorganisation_du_site_web_flyers_checkbox: boolean = false;
  copier_le_contenu_et_lorganisation_du_site_web_flyers_field: string = '';
  flyers_push_checkbox: boolean = false;
  flyers_push_field: string = '';
  news_push_checkbox: boolean = false;
  news_push_field: string = '';
  photos_a_nous_fournir_checkbox: boolean = false;
  photos_a_nous_fournir_field: string = '';
  videos_hebergees_sur_youtube_checkbox: boolean = false;
  videos_hebergees_sur_youtube_field: string = '';
  module_type_produits_checkbox1: boolean = false;
  module_type_produits_field1: string = '';
  module_type_produits_checkbox2: boolean = false;
  module_type_produits_field2: string = '';
  module_type_produits_checkbox3: boolean = false;
  module_type_produits_field3: string = '';
  panier: boolean = false;
  compte_paypal: string = '';
  activer_livraison: boolean = false;
  avec_paiement_en_ligne: boolean = false;
  avec_paiement_a_la_livraison: boolean = false;
  accepter_cb: boolean = false;
  accepter_cheques: boolean = false;
  accepter_tr: boolean = false;
  accepter_especes: boolean = false;
  frais_de_livraison_checkbox: boolean = false;
  frais_de_livraison_field: string = '';
  pour_letranger_europe: string = '';
  pour_letranger_reste_du_monde: string = '';
  minimum_de_commande_checkbox: boolean = false;
  minimum_de_commande_field: string = '';
  minimum_a_partir_duquel_les_frais_de_livraison_sont_gratuits_checkbox: boolean = false;
  minimum_a_partir_duquel_les_frais_de_livraison_sont_gratuits_field: string = '';
  activer_la_possiblite_detre_livre_des_que_possible: boolean = false;
  horaires_de_livraisons: string = '';
  activer_le_retrait_sur_place: boolean = false;
  avec_paiement_en_ligne2: boolean = false;
  avec_paiement_sur_place: boolean = false;
  avec_paiement_sur_place_accepter_cb: boolean = false;
  avec_paiement_sur_place_accepter_cheques: boolean = false;
  avec_paiement_sur_place_accepter_tr: boolean = false;
  avec_paiement_sur_place_accepter_especes: boolean = false;
  horaires_de_douvertures: string = '';
  visite_virtuelle_checkbox: boolean = false;
  visite_virtuelle_field: string = '';
  infos_contact_checkbox: boolean = false;
  infos_contact_field: string = '';
  formulaires_reservations: boolean = false;
  formulaires_devis: boolean = false;
  formulaires_rdv: boolean = false;
  formulaires_contact: boolean = false;
  formulaires_empty_field1: string = '';
  formulaires_empty_checkbox1: boolean = false;
  formulaires_empty_field2: string = '';
  formulaires_empty_checkbox2: boolean = false;
  horaires_douverture: string = '';
  module_de_type_texte_checkbox1: boolean = false;
  module_de_type_texte_string1: string = '';
  module_de_type_texte_checkbox2: boolean = false;
  module_de_type_texte_string2: string = '';
  module_de_type_texte_checkbox3: boolean = false;
  module_de_type_texte_string3: string = '';
  compte_client: boolean = false;
  compte_client_inscription_optionnelle: boolean = false;
  compte_client_inscription_obligatoire: boolean = false;
  compte_client_acces_prive: boolean = false;
  nous_fournir_les_cgv: string = '';
  geoloc_adresse_checkbox: boolean = false;
  geoloc_adresse_field: string = '';
  avis: boolean = false;

  // SITE INTERNET
  site_e_commerce: boolean = false;
  site_vitrine: boolean = false;
  nom_de_domaine_choix1: string = '';
  nom_de_domaine_choix2: string = '';
  nom_de_domaine_choix3: string = '';
  integrer_les_memes_modules_que_lapplication: boolean = false;
  remarques2: string = '';

  // SIGNATURES
  // LE CLIENT
  client_le1: string = '';
  client_le2: string = '';
  client_le3: string = '';
  client_signataire: string = '';
  client_qualite: string = '';
  // LA SOCIETE
  societe_le1: string = '';
  societe_le2: string = '';
  societe_le3: string = '';
  societe_signataire: string = '';
  societe_qualite: string = '';
}
