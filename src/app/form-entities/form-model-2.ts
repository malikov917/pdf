/**
 * Created by user1 on 3/24/2017.
 */
export class FormModel2 {

  apple_checkbox: boolean = false;
  android_checkbox: boolean = false;
  site_internet_checkbox: boolean = false;

  // CRÉATION D’APPLICATION MOBILE
  total_ht1: string = '';
  partenaire_checkbox: boolean = false;
  engagement_de_coup_de_fil: boolean = false;
  lettre_dor_utilisateurs: boolean = false;
  autorisation_interview_reportage: boolean = false;
  confidentialite_conditions: boolean = false;

  date: string = '';
  dossier_de_partenariat: string = '';

  // HÉBERGEMENT ET MAINTENANCE
  total_ht2: string = '';

  // Cahier des charges
  raison_sociale: string = '';
  nom_de_l_application: string = '';
  representee_par: string = '';
  fonction: string = '';
  adresse: string = '';
  code_postal: string = '';
  ville: string = '';
  telephone: string = '';
  email: string = '';
  site_internet: string = '';

}
