/**
 * Created by Konstantin on 19.03.2017.
 */
export class FormModel1 {

  raison_sociale: string = '';
  adresse: string = '';
  code_postal: string = '';
  ville: string = '';
  tel: string = '';
  fax: string = '';
  mail: string = '';
  site: string = '';
  siret: string = '';
  interlocuteur: string = '';
  applications_mobile: string = '';
  sites_web: string = '';
  apple: boolean = false;
  android: boolean = false;

  nomblre_de_mensualites: string = '';
  montant: string = '';
  montant2: string = '';
  tva: string = '';
  tva2: string = '';
  ttc: string = '';
  ttc2: string = '';
  cb: boolean = false;
  cheque: boolean = false;
  prelevement: boolean = false;

  le_client: boolean = false;
  le_client_le: string = '';
  le_client_a: string = '';
  le_client_signataire: string = '';
  le_client_qualite: string = '';

  la_societe_le: string = '';
  la_societe_a: string = '';
  la_societe_signataire: string = '';
  la_societe_qualite: string = '';

}
