import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pdf6Component } from './pdf-6.component';

describe('Pdf6Component', () => {
  let component: Pdf6Component;
  let fixture: ComponentFixture<Pdf6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pdf6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pdf6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
