import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {FormModel1} from "../../form-entities/form-model-1";

@Component({
  selector: 'app-pdf-1',
  templateUrl: './pdf-1.component.html',
  styleUrls: ['./pdf-1.component.css']
})
export class Pdf1Component implements OnInit {
  //form : FormModel1 = new FormModel1;

  apple = false;
  android = false;
  cb = false;
  cheque = false;
  prelevement = false;
  le_client = false;
  @Input() form1;
  @Output() countChange: EventEmitter<string>;

  constructor() {
    this.countChange = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  changed(string : string, e) {
    this.form1[string] = e;
    console.log('changed()');
    console.log(this.form1);
    this.countChange.emit(e);
  }

}
