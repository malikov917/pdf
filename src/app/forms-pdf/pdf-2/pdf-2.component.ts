import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pdf-2',
  templateUrl: './pdf-2.component.html',
  styleUrls: ['./pdf-2.component.css']
})
export class Pdf2Component implements OnInit {

  apple = false;
  android = false;
  site = false;
  @Input() form2;
  @Output() countChange: EventEmitter<string>;

  constructor() {
    this.countChange = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  changed(string : string, e) {
    this.form2[string] = e;
    console.log('changed()');
    console.log(this.form2);
    this.countChange.emit(e);
  }
}
