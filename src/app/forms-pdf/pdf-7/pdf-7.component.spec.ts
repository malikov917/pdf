import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pdf7Component } from './pdf-7.component';

describe('Pdf7Component', () => {
  let component: Pdf7Component;
  let fixture: ComponentFixture<Pdf7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pdf7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pdf7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
