import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pdf-3',
  templateUrl: './pdf-3.component.html',
  styleUrls: ['./pdf-3.component.css']
})
export class Pdf3Component implements OnInit {

  apple = false;
  android = false;

  cochez_cette_case_sil_ny_a_pas_de_site_web = false;
  copier_la_charte_graphique_du_site_web = false;
  copier_la_charte_graphique_du_flyer = false;
  copier_la_charte_graphique_du_carte_de_visite = false;
  reprendre_le_logo_du_site_web = false;
  reprendre_le_logo_du_flyer = false;
  reprendre_le_logo_du_carte_de_visite = false;
  copier_le_contenu_et_lorganisation_du_site_web_flyers_checkbox = false;
  flyers_push_checkbox = false;
  photos_a_nous_fournir_checkbox = false;
  videos_hebergees_sur_youtube_checkbox = false;
  module_type_produits_checkbox1 = false;
  module_type_produits_checkbox2 = false;
  module_type_produits_checkbox3 = false;
  panier = false;
  activer_livraison = false;
  avec_paiement_en_ligne = false;
  avec_paiement_a_la_livraison = false;
  accepter_cb = false;
  accepter_cheques = false;
  accepter_tr = false;
  accepter_especes = false;
  frais_de_livraison_checkbox = false;
  minimum_de_commande_checkbox = false;
  minimum_a_partir_duquel_les_frais_de_livraison_sont_gratuits_checkbox = false;
  activer_la_possiblite_detre_livre_des_que_possible = false;
  activer_le_retrait_sur_place = false;
  avec_paiement_en_ligne2 = false;
  avec_paiement_sur_place = false;
  avec_paiement_sur_place_accepter_cb = false;
  avec_paiement_sur_place_accepter_cheques = false;
  avec_paiement_sur_place_accepter_tr = false;
  avec_paiement_sur_place_accepter_especes = false;
  visite_virtuelle_checkbox = false;
  infos_contact_checkbox = false;
  formulaires_reservations = false;
  formulaires_devis = false;
  formulaires_rdv = false;
  formulaires_contact = false;
  formulaires_empty_checkbox1 = false;
  formulaires_empty_checkbox2 = false;
  module_de_type_texte_checkbox1 = false;
  module_de_type_texte_checkbox2 = false;
  module_de_type_texte_checkbox3 = false;
  compte_client = false;
  compte_client_inscription_optionnelle = false;
  compte_client_inscription_obligatoire = false;
  compte_client_acces_prive = false;
  geoloc_adresse_checkbox = false;
  avis = false;
  site_e_commerce = false;
  site_vitrine = false;
  integrer_les_memes_modules_que_lapplication = false;


  @Input() form3;
  @Output() countChange: EventEmitter<string>;

  constructor() {
    this.countChange = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  changed(string : string, e) {
    this.form3[string] = e;
    console.log('changed()');
    console.log(this.form3);
    this.countChange.emit(e);
  }

}
