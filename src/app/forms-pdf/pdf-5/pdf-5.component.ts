import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pdf-5',
  templateUrl: './pdf-5.component.html',
  styleUrls: ['./pdf-5.component.css']
})
export class Pdf5Component implements OnInit {

  @Input() form5;
  @Output() countChange: EventEmitter<string>;

  constructor() {
    this.countChange = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  changed(string : string, e) {
    this.form5[string] = e;
    console.log('changed()');
    console.log(this.form5);
    this.countChange.emit(e);
  }
}
