import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pdf5Component } from './pdf-5.component';

describe('Pdf5Component', () => {
  let component: Pdf5Component;
  let fixture: ComponentFixture<Pdf5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pdf5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pdf5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
