import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pdf-4',
  templateUrl: './pdf-4.component.html',
  styleUrls: ['./pdf-4.component.css']
})
export class Pdf4Component implements OnInit {

  @Input() form4;
  @Output() countChange: EventEmitter<string>;

  constructor() {
    this.countChange = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  changed(string : string, e) {
    this.form4[string] = e;
    console.log('changed()');
    console.log(this.form4);
    this.countChange.emit(e);
  }
}
