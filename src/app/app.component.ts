import {Component, Inject} from '@angular/core';
import * as jsPDF from 'jspdf';
import {FormModel1} from "./form-entities/form-model-1";
import {FormModel2} from "./form-entities/form-model-2";
import {Image1} from "./base64/image1";
import {Image2} from "./base64/image2";
import {Image3} from "./base64/image3";
import {Image4} from "./base64/image4";
import {Image5} from "./base64/image5";
import {FormModel3} from "./form-entities/form-model-3";
import {FormModel4} from "./form-entities/form-model-4";
import {FormModel5} from "./form-entities/form-model-5";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    {provide: 'Window', useValue: window}
  ]
})
export class AppComponent {

  form1: FormModel1 = new FormModel1();
  form2: FormModel2 = new FormModel2();
  form3: FormModel3 = new FormModel3();
  form4: FormModel4 = new FormModel4();
  form5: FormModel5 = new FormModel5();

  constructor(@Inject('Window') private window: Window,) {
    //this.form1 = new FormModel1;
  }

  download() {
    console.log("hohohoh!");
    console.log(btoa('./image/bdc recto_APPSVISION_v1-1.jpg'));

    let image1: Image1 = new Image1();
    let image2: Image2 = new Image2();
    let image3: Image3 = new Image3();
    let image4: Image4 = new Image4();
    let image5: Image5 = new Image5();

    let doc = new jsPDF();
    //doc.addPage();
    doc.addImage(image1.base64, 'JPEG', 0, 0, 210, 300);
    doc.setFontSize(10);
    if (this.form1.applications_mobile.length < 42) {
      doc.text(this.form1.applications_mobile, 19, 101.1);
    }
    if (this.form1.applications_mobile.length > 42) {
      doc.text(this.form1.applications_mobile.substring(0, 41), 19, 101.1);
      if (this.form1.applications_mobile.length > 82) {
        doc.text(this.form1.applications_mobile.substring(42, 81), 19, 108.6);
        if (this.form1.applications_mobile.length > 124) {
          doc.text(this.form1.applications_mobile.substring(82, 123), 19, 116.3);
          if (this.form1.applications_mobile.length > 166) {
            doc.text(this.form1.applications_mobile.substring(124, 165), 19, 124);
          }
        }
      }
    }

    if (this.form1.sites_web.length < 42) {
      doc.text(this.form1.sites_web, 110, 101.1);
    }
    if (this.form1.sites_web.length > 42) {
      doc.text(this.form1.sites_web.substring(0, 41), 110, 101.1);
      if (this.form1.sites_web.length > 82) {
        doc.text(this.form1.sites_web.substring(42, 81), 110, 108.6);
        if (this.form1.sites_web.length > 124) {
          doc.text(this.form1.sites_web.substring(82, 123), 110, 116.3);
          if (this.form1.sites_web.length > 166) {
            doc.text(this.form1.sites_web.substring(124, 165), 110, 124);
          }
        }
      }
    }


    doc.text(this.form1.sites_web, 110, 101.1);
    doc.text(this.form1.sites_web, 110, 108.6);
    doc.text(this.form1.sites_web, 110, 116.3);
    doc.text(this.form1.sites_web, 110, 124);
    //apple rect
    if (this.form1.apple) {
      doc.setFillColor(255, 0, 0);
      doc.rect(51.2, 142.5, 5.17, 3, 'F');
    }
    //android rect
    if (this.form1.android) {
      doc.setFillColor(255, 0, 0);
      doc.rect(117.8, 142.8, 5.17, 3, 'F');
    }

    doc.setFontSize(8);
    doc.text(this.form1.raison_sociale, 119, 21);
    doc.text(this.form1.adresse, 113, 26.5);
    doc.text(this.form1.adresse, 106, 32);

    doc.text(this.form1.code_postal, 116, 37.6);
    doc.text(this.form1.ville, 155, 37.6);
    doc.text(this.form1.fax, 154, 43.3);
    doc.text(this.form1.site, 154, 48.6);

    doc.text(this.form1.tel, 109, 43.3);
    doc.text(this.form1.mail, 110, 48.5);
    doc.text(this.form1.siret, 110.5, 54.2);
    doc.text(this.form1.interlocuteur, 119, 59.7);

    doc.setFontSize(10);
    doc.text(this.form1.nomblre_de_mensualites, 52.5, 190);
    //cadre contractuel
    doc.text(this.form1.montant, 37.5, 198.4);
    doc.text(this.form1.tva, 62.5, 198.4);
    doc.text(this.form1.ttc, 86.5, 198.4);
    //frais de dossier
    doc.text(this.form1.tva2, 117.5, 198.4);
    doc.text(this.form1.ttc2, 138.5, 198.4);

    doc.text(this.form1.montant, 128, 182);
    doc.text(this.form1.montant2, 109, 190.5);

    doc.setFillColor(255, 0, 0);
    // cb
    if (this.form1.cb) {
      doc.circle(173.8, 182.25, 1, 'F');
    }
    //cheque
    if (this.form1.cheque) {
      doc.circle(174.18, 190.4, 1, 'F');
    }
    //prelevement
    if (this.form1.prelevement) {
      doc.circle(173.75, 199.6, 1, 'F');
    }
    //en cochant cette case je demande le lancem
    if (this.form1.le_client) {
      doc.circle(19.57, 227.6, 1, 'F');
    }
    //LE CLIENT
    doc.text(this.form1.le_client_le, 23, 234.3);
    doc.text(this.form1.le_client_a, 64, 234.3);
    doc.text(this.form1.le_client_signataire, 34, 239.7);
    doc.text(this.form1.le_client_qualite, 30.5, 245.7);
    //LE SOCIETE
    doc.text(this.form1.la_societe_le, 113, 239);
    doc.text(this.form1.la_societe_a, 153.5, 239);
    doc.text(this.form1.la_societe_signataire, 124.2, 244.4);
    doc.text(this.form1.la_societe_qualite, 120.2, 250.2);

    //doc.addPage();


    var doc2 = new jsPDF({orientation: 'landscape', format: [300, 420]});

    doc2.addImage(image2.base64, 'JPEG', 0, 0, 420, 300);

    //apple
    if (this.form2.apple_checkbox) {
      doc2.setFillColor(255, 0, 0);
      doc2.rect(34.5, 16.5, 5.17, 5.2, 'F');
    }

    //android
    if (this.form2.android_checkbox) {
      doc2.setFillColor(255, 0, 0);
      doc2.rect(97, 16.5, 5.17, 5.2, 'F');
    }

    //site
    if (this.form2.site_internet_checkbox) {
      doc2.rect(160.8, 16.5, 5.17, 5.2, 'F');
      doc2.setFillColor(255, 0, 0);
    }
    doc2.text(this.form2.total_ht1, 160, 54);


    if (this.form2.partenaire_checkbox) {
      //check1 yes
      doc2.setFillColor(255, 0, 0);
      doc2.rect(153, 93.8, 3.8, 2.4, 'F');
    } else {
      //check1 no
      doc2.setFillColor(255, 0, 0);
      doc2.rect(177, 93.8, 3.8, 2.4, 'F');
    }

    //check2 yes
    if (this.form2.engagement_de_coup_de_fil) {
      doc2.setFillColor(255, 0, 0);
      doc2.rect(153, 99, 3.8, 2.4, 'F');
    } else {
      //check2 no
      doc2.setFillColor(255, 50, 0);
      doc2.rect(177, 99, 3.8, 2.4, 'F');
    }
    if (this.form2.lettre_dor_utilisateurs) {
      //check3 yes
      doc2.setFillColor(255, 0, 0);
      doc2.rect(153, 104.3, 3.8, 2.4, 'F');
    } else {
      //check3 no
      doc2.setFillColor(255, 50, 0);
      doc2.rect(177, 104.3, 3.8, 2.4, 'F');
    }
    if (this.form2.autorisation_interview_reportage) {
      //check4 yes
      doc2.setFillColor(255, 0, 0);
      doc2.rect(153, 109.5, 3.8, 2.4, 'F');
    } else {
      //check4 no
      doc2.setFillColor(255, 50, 0);
      doc2.rect(177, 109.5, 3.8, 2.4, 'F');
    }
    if (this.form2.confidentialite_conditions) {
      //check5 yes
      doc2.setFillColor(255, 0, 0);
      doc2.rect(153, 114.7, 3.8, 2.4, 'F');
    } else {
      //check5 no
      doc2.setFillColor(255, 50, 0);
      doc2.rect(177, 114.7, 3.8, 2.4, 'F');
    }
    doc2.text(this.form2.total_ht2, 160, 200);

    //date
    doc2.text(this.form2.date, 330, 58);
    //dossier_de_partenariat
    doc2.text(this.form2.dossier_de_partenariat, 330, 69);

    //Raison sociale
    doc2.text(this.form2.raison_sociale, 325, 163);
    //Nom de app
    doc2.text(this.form2.nom_de_l_application, 321, 176);

    //rep par
    doc2.text(this.form2.representee_par, 317, 191);
    //fonction
    doc2.text(this.form2.fonction, 313, 202);

    //addresse
    doc2.text(this.form2.adresse, 309, 212);
    //code postal
    doc2.text(this.form2.code_postal, 305, 222);
    //ville
    doc2.text(this.form2.ville, 301, 233);
    //tel
    doc2.text(this.form2.telephone, 297, 246);
    //email
    doc2.text(this.form2.email, 293, 258);
    //site
    doc2.text(this.form2.site_internet, 289, 269);

    doc2.addPage();

    doc2.addImage(image3.base64, 'JPEG', 0, 0, 420, 300);

    //1
    doc2.setFontSize(10);
    doc2.text(this.form3.nom_prenom_du_contact, 58, 36);
    //2
    doc2.text(this.form3.numero_de_portable, 50, 41.1);
    //3
    doc2.text(this.form3.numero_de_fixe, 45, 46.4);
    //4
    doc2.text(this.form3.mail_ce_mail_recevra_les_demandes_faites_via_lapplication, 95, 51.8);
    //5
    doc2.text(this.form3.site_internet, 37, 57.2);

    if (this.form3.cochez_cette_case_sil_ny_a_pas_de_site_web) {
      doc2.setFillColor(255, 0, 0);
      doc2.rect(10.5, 60.2, 3.1, 3.1, 'F');
    }

    //facebook
    doc2.text(this.form3.adresses_facebook, 43, 73.2);

    //twitter
    doc2.text(this.form3.twitter, 43, 78.4);

    //pinterest
    doc2.text(this.form3.pinterest, 43, 84);

    //google plus
    doc2.text(this.form3.google_plus, 50, 89.7);

    //inst
    doc2.text(this.form3.instagram, 130, 84);

    //tripadvisor
    doc2.text(this.form3.tripadvisor, 130, 89.7);

    //npm de
    doc2.text(this.form3.numero_de_portable, 10, 105);

    //mot
    doc2.text(this.form3.mots_cles, 10, 116);

    if (this.form3.visite_virtuelle_checkbox) {
      //site check
      doc2.setFillColor(255, 0, 0);
      doc2.rect(66.4, 137.9, 3.1, 3.1, 'F');
    }
    if (this.form3.flyers_push_checkbox) {
      //flyer
      doc2.setFillColor(255, 0, 0);
      doc2.rect(89.8, 137.9, 3.1, 3.1, 'F');
    }
    if (this.form3.copier_la_charte_graphique_du_carte_de_visite) {
      //carte de viste
      doc2.setFillColor(255, 0, 0);
      doc2.rect(107.3, 137.9, 3.1, 3.1, 'F');
    }

    //couleur1_1
    doc2.text(this.form3.couleur_1_1, 30, 157.5);
    //couleur1_2
    doc2.text(this.form3.couleur_1_2, 112, 157.5);
    //couleur1_3
    doc2.text(this.form3.couleur_1_3, 128, 157.5);
    //couleur1_4
    doc2.text(this.form3.couleur_1_4, 172, 157.5);

    //couleur2_1
    doc2.text(this.form3.couleur_2_1, 30, 161.8);
    //couleur2_2
    doc2.text(this.form3.couleur_2_2, 112, 161.8);
    //couleur2_3
    doc2.text(this.form3.couleur_2_3, 128, 161.8);
    //couleur2_4
    doc2.text(this.form3.couleur_2_4, 172, 161.8);

    //couleur3_1
    doc2.text(this.form3.couleur_3_1, 30, 166);
    //couleur3_2
    doc2.text(this.form3.couleur_3_2, 112, 166);
    //couleur3_3
    doc2.text(this.form3.couleur_3_3, 128, 166);
    //couleur3_4
    doc2.text(this.form3.couleur_3_4, 172, 166);

    if (this.form3.reprendre_le_logo_du_site_web) {
      //site web
      doc2.setFillColor(255, 0, 0);
      doc2.rect(50.45, 172.05, 3.1, 3.1, 'F');
    }
    if (this.form3.reprendre_le_logo_du_flyer) {
      //flyer
      doc2.setFillColor(255, 0, 0);
      doc2.rect(73.3, 172.05, 3.1, 3.1, 'F');
    }
    if (this.form3.reprendre_le_logo_du_carte_de_visite) {
      //carte de viste
      doc2.setFillColor(255, 0, 0);
      doc2.rect(89.8, 172.05, 3.1, 3.1, 'F');
    }

    if (this.form1.applications_mobile.length > 42) {
      doc.text(this.form1.applications_mobile.substring(0, 41), 19, 101.1);
      if (this.form1.applications_mobile.length > 82) {
        doc.text(this.form1.applications_mobile.substring(42, 81), 19, 108.6);
        if (this.form1.applications_mobile.length > 124) {
          doc.text(this.form1.applications_mobile.substring(82, 123), 19, 116.3);
          if (this.form1.applications_mobile.length > 166) {
            doc.text(this.form1.applications_mobile.substring(124, 165), 19, 124);
          }
        }
      }
    }

    //logotextleft
    if (this.form3.creer_un_logo.length <= 42)
      doc2.text(this.form3.creer_un_logo, 30, 188);
    if (this.form3.creer_un_logo.length > 42) {
      doc2.text(this.form3.creer_un_logo.substring(0,41), 30, 188);
      if (this.form3.creer_un_logo.length > 82){
        doc2.text(this.form3.creer_un_logo.substring(42, 81), 10, 192);
        if (this.form3.creer_un_logo.length > 124){
          doc2.text(this.form3.creer_un_logo.substring(82,123), 10, 197);
        }
      }
    }
    //logotextleft2
    if (this.form3.fonds_decrans.length <= 42){
      doc2.text(this.form3.fonds_decrans, 10, 215);
    }
    if (this.form3.fonds_decrans.length > 42){
      doc2.text(this.form3.fonds_decrans.substring(0,41), 10, 215);
      if (this.form3.fonds_decrans.length > 82){
        doc2.text(this.form3.fonds_decrans.substring(42,81), 10, 220);
        if (this.form3.fonds_decrans.length > 124){
          doc2.text(this.form3.fonds_decrans.substring(82,123), 10, 225);
        }
      }
    }



    //logotextleft3
    doc2.text(this.form3.nom_de_la_les_typographies_a_utiliser, 85, 269);
////////////// ПЕРЕХОД
    //logotextleft4
    if (this.form3.remarques.length <= 42){
      doc2.text(this.form3.remarques, 31, 279.5);

    }
    if (this.form3.remarques.length > 42){
      doc2.text(this.form3.remarques.substring(0,41), 31, 279.5);
      if (this.form3.remarques.length > 82){
        doc2.text(this.form3.remarques.substring(42,81), 10, 284);
        if (this.form3.remarques.length > 124){
          doc2.text(this.form3.remarques.substring(82,123), 10, 289);
        }
      }
    }
    doc2.setFillColor(255, 0, 0);
    if (this.form3.copier_le_contenu_et_lorganisation_du_site_web_flyers_checkbox)
      doc2.rect(221.9, 32.15, 3, 3, 'F');

    if (this.form3.flyers_push_checkbox)
      doc2.rect(221.9, 40.15, 3, 3, 'F');
    if (this.form3.news_push_checkbox)
      doc2.rect(221.9, 45.15, 3, 3, 'F');
    if (this.form3.module_type_produits_checkbox1)
      doc2.rect(221.9, 53.15, 3, 3, 'F');
    if (this.form3.module_type_produits_checkbox2)
      doc2.rect(221.9, 58.15, 3, 3, 'F');
    if (this.form3.module_type_produits_checkbox3)
      doc2.rect(221.9, 62.15, 3, 3, 'F');
    if (this.form3.panier)
      doc2.rect(221.9, 70.15, 3, 3, 'F');
    if (this.form3.visite_virtuelle_checkbox)
      doc2.rect(221.9, 126.15, 3, 3, 'F');
    if (this.form3.infos_contact_checkbox)
      doc2.rect(221.9, 131.15, 3, 3, 'F');
    if (this.form3.module_de_type_texte_checkbox1)
      doc2.rect(221.9, 152.15, 3, 3, 'F');
    if (this.form3.module_de_type_texte_checkbox2)
      doc2.rect(221.9, 157.15, 3, 3, 'F');
    if (this.form3.module_de_type_texte_checkbox3)
      doc2.rect(221.9, 161.15, 3, 3, 'F');
    if (this.form3.compte_client)
      doc2.rect(221.9, 168.15, 3, 3, 'F');
    if (this.form3.geoloc_adresse_checkbox)
      doc2.rect(221.9, 179.15, 3, 3, 'F');
    if (this.form3.avis)
      doc2.rect(221.9, 186.15, 3, 3, 'F');
    if (this.form3.activer_livraison)
      doc2.rect(230, 76, 2, 2, 'F');
    if (this.form3.avec_paiement_en_ligne)
      doc2.rect(260, 76, 2, 2, 'F');
    if (this.form3.avec_paiement_a_la_livraison)
      doc2.rect(260, 80, 2, 2, 'F');
    if (this.form3.accepter_cb)
      doc2.rect(321, 80, 2, 2, 'F');
    if (this.form3.accepter_cheques)
      doc2.rect(329, 80, 2, 2, 'F');
    if (this.form3.accepter_tr)
      doc2.rect(346, 80, 2, 2, 'F');
    if (this.form3.accepter_especes)
      doc2.rect(353.5, 80, 2, 2, 'F');
    if (this.form3.frais_de_livraison_checkbox)
      doc2.rect(260.5, 87, 2, 2, 'F');
    if (this.form3.minimum_de_commande_checkbox)
      doc2.rect(260.5, 93, 2, 2, 'F');
    if (this.form3.minimum_a_partir_duquel_les_frais_de_livraison_sont_gratuits_checkbox)
      doc2.rect(260.5, 97, 2, 2, 'F');
    if (this.form3.activer_la_possiblite_detre_livre_des_que_possible)
      doc2.rect(260.5, 101, 2, 2, 'F');
    if (this.form3.avec_paiement_en_ligne2)
      doc2.rect(272, 110, 2, 2, 'F');
    if (this.form3.avec_paiement_sur_place)
      doc2.rect(272, 114, 2, 2, 'F');
    if (this.form3.accepter_cb)
      doc2.rect(328, 114, 2, 2, 'F');
    if (this.form3.accepter_cheques)
      doc2.rect(340, 114, 2, 2, 'F');
    if (this.form3.accepter_tr)
      doc2.rect(357, 114, 2, 2, 'F');
    if (this.form3.accepter_especes)
      doc2.rect(367, 114, 2, 2, 'F');


    if (this.form3.compte_client_inscription_optionnelle)
      doc2.rect(257.5, 169, 2.5, 2.5, 'F');
    if (this.form3.compte_client_inscription_obligatoire)
      doc2.rect(295.5, 169, 2.5, 2.5, 'F');
    if (this.form3.compte_client_acces_prive)
      doc2.rect(332.5, 169, 2.5, 2.5, 'F');

    if (this.form3.formulaires_reservations)
      doc2.rect(246, 137.1, 2, 2, 'F');
    if (this.form3.formulaires_devis)
      doc2.rect(274.8, 137, 2, 2, 'F');
    if (this.form3.formulaires_rdv)
      doc2.rect(292, 137, 2, 2, 'F');
    if (this.form3.formulaires_contact)
      doc2.rect(306, 137, 2, 2, 'F');
    if (this.form3.formulaires_empty_checkbox1)
      doc2.rect(329, 137, 2, 2, 'F');
    if (this.form3.formulaires_empty_checkbox2)
      doc2.rect(368, 137, 2, 2, 'F');

    if (this.form3.activer_le_retrait_sur_place)
      doc2.rect(230, 111, 2, 2, 'F');
    if (this.form3.site_e_commerce)
      doc2.rect(253, 206, 2.5, 2.5, 'F');
    if (this.form3.site_vitrine)
      doc2.rect(294, 206, 2.5, 2.5, 'F');
    if (this.form3.integrer_les_memes_modules_que_lapplication)
      doc2.rect(222.5, 234.5, 2.5, 2.5, 'F');

    if (this.form3.photos_a_nous_fournir_checkbox)
      doc2.rect(308.9, 40.15, 3, 3, 'F');
    if (this.form3.videos_hebergees_sur_youtube_checkbox)
      doc2.rect(308.9, 45.15, 3, 3, 'F');

    doc2.setFontSize(10);

    doc2.text(this.form3.flyers_push_field, 248, 42.5);
    doc2.text(this.form3.photos_a_nous_fournir_field, 347, 42.5);

    doc2.text(this.form3.videos_hebergees_sur_youtube_field, 361, 46.5);
    doc2.text(this.form3.news_push_field, 248, 46.5);

    doc2.text(this.form3.module_type_produits_field1, 316, 56);
    doc2.text(this.form3.module_type_produits_field2, 292, 60);

    doc2.text(this.form3.module_type_produits_field3, 292, 65);

    doc2.text(this.form3.compte_paypal, 320, 35);

    doc2.setFontSize(7);
    doc2.text(this.form3.frais_de_livraison_field, 288, 88);
    doc2.text(this.form3.pour_letranger_europe, 353, 88);
    doc2.text(this.form3.pour_letranger_reste_du_monde, 347, 92);
    doc2.text(this.form3.minimum_de_commande_field, 300, 95);
    doc2.text(this.form3.minimum_a_partir_duquel_les_frais_de_livraison_sont_gratuits_field, 342, 99);
    doc2.text(this.form3.horaires_de_livraisons, 264, 106);
    doc2.setFontSize(10);
    doc2.text(this.form3.compte_paypal, 341, 72);
    doc2.text(this.form3.horaires_de_douvertures, 269, 120);

    doc2.text(this.form3.visite_virtuelle_field, 279, 129);
    doc2.text(this.form3.infos_contact_field, 255, 133);
    doc2.text(this.form3.formulaires_empty_field1, 333, 139);
    doc2.text(this.form3.formulaires_empty_field2, 373, 139);
    if (this.form3.horaires_douverture.length <= 42){
      doc2.text(this.form3.horaires_douverture, 258, 143);
    }
    if (this.form3.horaires_douverture.length > 42){
      doc2.text(this.form3.horaires_douverture.substring(0, 41), 258, 143);
      if (this.form3.horaires_douverture.length > 82){
        doc2.text(this.form3.horaires_douverture.substring(42, 81), 221, 147);
      }
    }

    doc2.text(this.form3.module_de_type_texte_string1, 318, 155);
    doc2.text(this.form3.module_de_type_texte_string2, 294, 160);
    doc2.text(this.form3.module_de_type_texte_string3, 294, 164);
    doc2.text(this.form3.nous_fournir_les_cgv, 358, 175);
    doc2.text(this.form3.geoloc_adresse_field, 264, 182);
    doc2.text(this.form3.nom_de_domaine_choix1, 270, 217.5);
    doc2.text(this.form3.nom_de_domaine_choix2, 270, 223);
    doc2.text(this.form3.nom_de_domaine_choix3, 270, 228);
    doc2.text(this.form3.remarques2, 243.5, 244.5);
    doc2.text(this.form3.client_le1, 227.5, 268);
    doc2.text(this.form3.client_le2, 248.5, 268);
    doc2.text(this.form3.client_le3, 268.5, 268);

    doc2.text(this.form3.client_signataire, 242.5, 272);
    doc2.text(this.form3.client_qualite, 238, 277);

    doc2.text(this.form3.societe_le1, 325.5, 268);
    doc2.text(this.form3.societe_le2, 346.5, 268);
    doc2.text(this.form3.societe_le3, 366.5, 268);

    doc2.text(this.form3.societe_signataire, 340.5, 272);
    doc2.text(this.form3.societe_qualite, 335.5, 277);

    //let doc3 = new jsPDF();
    doc.addPage();
    doc.addImage(image4.base64, 'JPEG', 0, 0, 210, 300);
    doc.text(this.form4.iban, 30, 221.7);
    doc.text(this.form4.bic, 30, 227);
    doc.text(this.form4.tiers_debiteur, 45, 237.2);
    doc.text(this.form4.a, 25, 247.3);
    doc.text(this.form4.le, 25, 252.55);
    doc.text(this.form4.ics, 126, 238);
    doc.text(this.form4.rum, 127, 243);
    //doc.save('PDFA4_5');

    doc.addPage();

    doc.setFontSize(40);
    doc.text(35, 25, 'Paranyan loves jsPDF');
    doc.addImage(image5.base64, 'JPEG', 0, 0, 210, 300);

    doc.setFontSize(18);
    doc.text(this.form5.versions_des_logiciels, 15, 195);
    doc.text(this.form5.site_web, 15, 225);
    doc.setFontSize(10);
    doc.text(this.form5.le_locataire_a, 21, 256);
    doc.text(this.form5.le_locataire_le, 21, 260.9);
    doc.text(this.form5.le_fournisseur_a, 113, 256);
    doc.text(this.form5.le_fournisseur_le, 113, 260.9);

    doc2.save('PDF_A3');

    doc.save('PDF');

  }
}
