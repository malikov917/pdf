import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { Pdf1Component } from './forms-pdf/pdf-1/pdf-1.component';
import { Pdf2Component } from './forms-pdf/pdf-2/pdf-2.component';
import { Pdf3Component } from './forms-pdf/pdf-3/pdf-3.component';
import { Pdf4Component } from './forms-pdf/pdf-4/pdf-4.component';
import { Pdf5Component } from './forms-pdf/pdf-5/pdf-5.component';
import { Pdf6Component } from './forms-pdf/pdf-6/pdf-6.component';
import { Pdf7Component } from './forms-pdf/pdf-7/pdf-7.component';
import {SqueezeBoxModule} from 'squeezebox';
import {MaterialModule} from "@angular/material";

@NgModule({
  declarations: [
    AppComponent,
    Pdf1Component,
    Pdf2Component,
    Pdf3Component,
    Pdf4Component,
    Pdf5Component,
    Pdf6Component,
    Pdf7Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SqueezeBoxModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
